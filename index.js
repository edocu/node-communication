const kafka = require('kafka-node')
const _ = require('lodash')

const Consumer = kafka.ConsumerGroup
const Producer = kafka.HighLevelProducer
const KeyedMessage = kafka.KeyedMessage

const Communicator = function(options) {
    const client = new kafka.Client(options.url || 'zookeeper.service.dev.edocu.local:2181')
    const cbrTopic = 'system.router'
    const consumerOptions = {
        autoCommit: true,
        encoding: 'buffer',
        groupId: options.groupId || 'node-test'
    }

    const producer = new Producer(client, {
            partitionerType: 2 // cyclic
        });

    producer.on('error', (e) => {
        console.error(e)
    })

    const producerReadyPromise = new Promise((resolve) => {
        producer.on('ready', () => {
            resolve()
        })
    })
    

    return {
        onMessage,
        postMessage 
    }

    function onMessage(topic, cb) {
        createConsumer(topic)
        .then((consumer) => {
            consumer.on('message', function(message) {
                let messageValue
                let topicValue

                try {
                    messageValue = JSON.parse(message.value)
                }
                catch (e) {
                    messageValue = {}
                }

                topicValue = (message.key !== -1) ? message.key.toString() : message.topic

                cb(topicValue, messageValue)
            });
            consumer.on('error', function(error) {
                console.error(error);
            });

            console.log(`Consumer "${consumerOptions.groupId}" started listening on topic "${topic}"`)

            // process.on('exit', close);
            // process.on('SIGINT', close);
            // function close() {
            //     console.log('gracefully closing consumer')
            //     consumer.close(process.exit)
            // }
        })
        .catch((e) => {
            console.error(e)
        })
    }

    function postMessage(topic, message) {
        var km = new KeyedMessage(topic, JSON.stringify(message))
        return producerReadyPromise
        .then(function() {
            return post(km)
        })
    }

    function createConsumer(topic) {
        return registerTopic(topic)
        .then(function() {
            return generateConsumer(topic);
        })
    }

    function generateConsumer(topic) {
        return new Consumer({
            host: options.url || 'zookeeper.service.dev.edocu.local:2181',
            groupId: options.groupId || 'node-test',
            sessionTimeout: 15000,
            protocol: ['roundrobin']
        }, topic)
    }

    function registerTopic(topic) {
        const topicToRegister = topic.replace(/_ANY_/g, '*')
        const km = new KeyedMessage(cbrTopic + '.register-topic', JSON.stringify({topic: topicToRegister}))

        return producerReadyPromise
        .then(() => {
            return post(km)
        })
        .then(() => {
            return isTopicRegistered(topic)
        })
    }

    function isTopicRegistered(topic) {
        return new Promise((resolve, reject) => {
            client.zk.topicExists(topic, (err, exists, topic) => {
                if (err) {
                    reject(err);
                } else {
                    if (exists) {
                        // Ensure leader selection happened
                        setTimeout(resolve, 1000);
                    }
                }
            }, true);
        });
    }

    function post(km) {
        return new Promise((resolve) => {
            producer.send([{
                topic: cbrTopic,
                messages: [km],
                partition: 1
            }], function(data) {
                if (data) {
                    resolve(_.values(data[cbrTopic]));
                } else {
                    resolve();
                }
            });
        })
    }
}

module.exports = Communicator