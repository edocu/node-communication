var Communicator = require('./index.js');

var communicator = Communicator({
    groupId: 'example.js'
});

communicator.onMessage('lorem.ipsum.dolor.sit.z', function(topic, message) {
    console.log('Topic', topic);
    console.log('Message', message);
});

communicator.postMessage('lorem.ipsum.dolor.sit.z', {foo: 'sky'})
.then(function(data) {
    console.log('message send to topic lorem.ipsum.dolor.sit.z');
})
.catch(function(error) {
    console.log(error);
});


// process.stdin.setEncoding('utf8');
//
// process.stdin.on('data', (chunk) => {
//     console.log('resend message');
//     communicator.postMessage('lorem.ipsum.dolor.sit.j', {foo: 'sky'})
//     .then(function(data) {
//         console.log("Data", data);
//     })
//     .catch(function(error) {
//         console.log(error);
//     });
// })
//
// process.stdin.on('end', () => {
//   process.stdout.write('end');
//   process.exit();
// });
