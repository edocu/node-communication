var chai = require('chai'),
    should = chai.should(),
    chaiAsPromised = require('chai-as-promised');
    sinon = require('sinon');

chai.use(chaiAsPromised);

var Communicator = require('./index.js');

var options = {
    groupId: 'foosky',
    url: 'zookeeper:2181'
}

var topicMock = 'lorem.ipsum',
    messageMock = {foo: 'sky'};

describe('Communicator', function() {
    var communicator;

    beforeEach(function() {
        communicator = Communicator(options);
    });

    it('should create without errors', function() {
        communicator.should.be.defined;
    });

    it('should apply options', function() {
        communicator._.client.connectionString.should.equal(options.url);
        communicator._.consumerOptions.groupId.should.equal(options.groupId);
    });

    it('should work without passing any option', function() {
        communicator = Communicator();
        communicator.should.be.defined;
    });

    describe('Producer', function() {
        var producerSendStub;

        beforeEach(function() {
            producerSendStub = sinon.stub(communicator._.producer, 'send');
        });

        it('should not post when producer is not ready', function() {
            communicator.postMessage(topicMock, messageMock);
            producerSendStub.called.should.be.false;
        });

        it('should post message when producer is ready', function() {
            var postResponse = {
                'system.router': {
                    '0': 25
                }
            };
            producerSendStub.yields(postResponse);

            communicator._.producerReady();

            return communicator.postMessage(topicMock, messageMock).should.eventually.have.property(0,25);
        });

        it('should continue posting when producer becomes ready', function(done) {
            communicator.postMessage(topicMock, messageMock);
            producerSendStub.called.should.be.false;

            communicator._.producer.emit('ready');
            setTimeout(function() {
                producerSendStub.calledOnce.should.be.true;
                done();
            },1)
        });

        it('should not post when producer becomes not ready', function() {
            communicator._.producerReady();
            communicator._.producerError('');
            communicator.postMessage(topicMock, messageMock);
            producerSendStub.called.should.be.false;
        });
    });

    describe('Consumer', function() {
        it('should be able to create consumer instance', function() {
            var postResponse = {
                'system.router': {
                    '0': 25
                }
            };
            var producerSendStub = sinon
                .stub(communicator._.producer, 'send')
                .yields(postResponse);

            var zkTopicExistsStub = sinon
                .stub(communicator._.client.zk, 'topicExists')
                .yields(null, true);

            communicator._.producerReady();

            return communicator._.createConsumer(topicMock).should.eventually.be.defined;
        });

        it('should be able to register topic when producer is ready', function() {
            var postResponse = {
                'system.router': {
                    '0': 25
                }
            };
            var producerSendStub = sinon
                .stub(communicator._.producer, 'send')
                .yields(postResponse);

            var zkTopicExistsStub = sinon
                .stub(communicator._.client.zk, 'topicExists')
                .yields(null, true);

            communicator._.producerReady();

            return communicator._.registerTopic(topicMock).should.be.fulfilled;
        });

        it('should be able to register topic when producer becomes ready later', function() {
            var postResponse = {
                'system.router': {
                    '0': 25
                }
            };
            var producerSendStub = sinon
                .stub(communicator._.producer, 'send')
                .yields(postResponse);

            var zkTopicExistsStub = sinon
                .stub(communicator._.client.zk, 'topicExists')
                .yields(null, true);

            var registerTopicPromise = communicator._.registerTopic(topicMock);

            registerTopicPromise.should.not.be.fulfilled;

            communicator._.producer.emit('ready');

            return registerTopicPromise.should.be.fulfilled;
        });


        it('should create topic when starting message listener', function() {

        });

        it('should not create consumer until topic is registered', function() {

        });

        it('should be able to listen to messages', function() {

        });
    });
});
